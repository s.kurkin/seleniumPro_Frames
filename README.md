Задание:
1) Открыть http://way2automation.com/way2auto_jquery/frames-and-windows.php
2) Открыть Open Multiple Windows
3) Нажать на ссылку
4) Перенести фокус на новую вкладку, нажать ссылку
5) Убедиться, что открылась третья вкладка

    
Запуск теста:
1) В файле \src\test\resources\config.properties заполнить проперти соответствующими значениями:  
way2.user.userName - логин пользователя
way2.user.password - пароль пользователя 	 

2) mvn clean -Dtest=FramesCase test