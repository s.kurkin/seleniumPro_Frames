package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class FramesPage extends BasePage {

    //Main menu
    @FindBy(xpath = "//li/a[contains(text(),'Open Multiple Windows')]")
    private WebElement mainMenuEl;

    //Link button
    @FindBy(xpath = ".//div[@class='farme_window']/p/a")
    private WebElement linkBtn;

    //Frame
    @FindBy(xpath = ".//div[@id='example-1-tab-4']//iframe")
    private WebElement frameMultiWindowEl;

    public FramesPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public WebElement getMainMenu() {
        return getElement(mainMenuEl);
    }

    public WebElement getFrameMultiWindow() {
        return getElement(frameMultiWindowEl);
    }

    public WebElement getLinkEl() {
        return getElement(linkBtn);
    }
}
