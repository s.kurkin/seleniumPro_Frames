package cases;

import io.github.bonigarcia.wdm.FirefoxDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.springframework.core.io.ClassPathResource;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.FramesPage;
import pages.LoginPage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import static org.testng.Assert.fail;


public class FramesCase {

    private final String LOGIN_PAGE_URL = "http://way2automation.com/way2auto_jquery/index.php";
    private final String FRAMES_PAGE_URL = "http://way2automation.com/way2auto_jquery/frames-and-windows.php";
    private String userName = "";
    private String userPassword = "";
    private WebDriver driver;
    private FramesPage framesPage;
    private LoginPage loginPage;

    @BeforeClass
    public void setUp() throws Exception {
        FirefoxDriverManager.getInstance().version("0.19.1").setup();
        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
        driver.manage().timeouts().setScriptTimeout(20, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        initProperties();
    }

    @Test
    public void testFrames() throws IOException {
        logIn();
        openMultiWindow();
        switchToThirdWindow();
        Assert.assertEquals(driver.findElement(By.xpath("//a[contains(text(),'Open Window-3')]")).getText(), "Open Window-3");
    }

    private void switchToThirdWindow() {
        //List of handl's ID
        ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(tabs.get(3));
    }

    private void openMultiWindow() {
        driver.get(FRAMES_PAGE_URL);
        framesPage = new FramesPage(driver);
        framesPage.clickElement(framesPage.getMainMenu());
        driver.switchTo().frame(framesPage.getFrameMultiWindow());
        framesPage.clickElement(framesPage.getLinkEl());
    }

    private void logIn() throws IOException {
        driver.get(LOGIN_PAGE_URL);
        loginPage = new LoginPage(driver);
        loginPage.signInBtnClick();
        loginPage.enterUserName(userName);
        loginPage.enterPassword(userPassword);
        loginPage.submitClick();
    }

    private void initProperties() throws IOException {
        Properties config = new Properties();
        try {
            config.load(new ClassPathResource("config.properties").getInputStream());
            userName = config.getProperty("way2.user.userName");
            userPassword = config.getProperty("way2.user.password");
        } catch (IOException ex) {
            ex.printStackTrace();
            fail("File 'config.properties' can't be open!");
        }
    }

    @AfterClass
    public void tearDown() throws Exception {
        driver.quit();
    }
}
